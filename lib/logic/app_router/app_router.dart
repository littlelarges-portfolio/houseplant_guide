import 'package:go_router/go_router.dart';
import 'package:houseplant_guide/logic/data/page/plant_page_data.dart';
import 'package:houseplant_guide/logic/data/page/plant_type_page_data.dart';
import 'package:houseplant_guide/logic/pages/plant_type_page.dart';

import '../pages/home_page.dart';
import '../pages/plant_page.dart';

class AppRouter {
  GoRouter router = GoRouter(routes: [
    GoRoute(
      path: '/',
      builder: (context, state) {
        return const HomePage(title: 'Houseplant Guide');
      },
    ),
    GoRoute(
      path: '/plantType',
      builder: (context, state) {
        PlantTypePageData plantTypePageData = state.extra as PlantTypePageData;

        return PlantTypePage(
          title: plantTypePageData.title,
          plantTypeIndex: plantTypePageData.plantTypeIndex,
        );
      },
    ),
    GoRoute(
      path: '/plant',
      builder: (context, state) {
        PlantPageData plantPageData = state.extra as PlantPageData;

        return PlantPage(
          title: plantPageData.title,
          plantTypeIndex: plantPageData.plantTypeIndex,
          plantIndex: plantPageData.plantIndex,
        );
      },
    ),
  ]);
}
