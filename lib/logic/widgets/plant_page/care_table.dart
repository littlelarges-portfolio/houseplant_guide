import 'package:flutter/material.dart';
import 'package:houseplant_guide/main.dart';

class CareTable extends StatelessWidget {
  const CareTable(
      {required this.plantTypeIndex, required this.plantIndex, super.key});

  final int plantTypeIndex, plantIndex;

  @override
  Widget build(BuildContext context) {
    return Table(
      border: TableBorder.all(),
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      columnWidths: const {
        0: FlexColumnWidth(.5),
      },
      children: [
        TableRow(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('Temperature:'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(appData.plantTypes[plantTypeIndex].plants[plantIndex]
                  .plantData.care.temperature),
            ),
          ],
        ),
        TableRow(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('Light:'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(appData.plantTypes[plantTypeIndex].plants[plantIndex]
                  .plantData.care.light),
            ),
          ],
        ),
        TableRow(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('Watering:'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(appData.plantTypes[plantTypeIndex].plants[plantIndex]
                  .plantData.care.watering),
            ),
          ],
        ),
        TableRow(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('Soil:'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(appData.plantTypes[plantTypeIndex].plants[plantIndex]
                  .plantData.care.soil),
            ),
          ],
        ),
        TableRow(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('Re-Potting:'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(appData.plantTypes[plantTypeIndex].plants[plantIndex]
                  .plantData.care.rePotting),
            ),
          ],
        ),
        TableRow(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('Humidity:'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(appData.plantTypes[plantTypeIndex].plants[plantIndex]
                  .plantData.care.humidity),
            ),
          ],
        ),
        TableRow(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('Propagation:'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(appData.plantTypes[plantTypeIndex].plants[plantIndex]
                  .plantData.care.propagation),
            ),
          ],
        ),
      ],
    );
  }
}
