import 'package:flutter/material.dart';
import 'package:houseplant_guide/main.dart';

class CommonProblems extends StatelessWidget {
  const CommonProblems(
      {required this.plantTypeIndex, required this.plantIndex, super.key});

  final int plantTypeIndex, plantIndex;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
          appData.plantTypes[plantTypeIndex].plants[plantIndex].plantData.commonProblems
              .length, (index) {
        return Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text('\u2022'),
            const SizedBox(width: 10),
            Expanded(
                child: Text(appData.plantTypes[plantTypeIndex]
                    .plants[plantIndex].plantData.commonProblems[index])),
          ],
        );
      }),
    );
  }
}
