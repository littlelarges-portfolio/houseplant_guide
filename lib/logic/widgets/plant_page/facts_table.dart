import 'package:flutter/material.dart';
import 'package:houseplant_guide/main.dart';

class FactsTable extends StatelessWidget {
  const FactsTable(
      {required this.plantTypeIndex, required this.plantIndex, super.key});

  final int plantTypeIndex, plantIndex;

  @override
  Widget build(BuildContext context) {
    return Table(
      border: TableBorder.all(),
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      columnWidths: const {
        0: FlexColumnWidth(.5),
      },
      children: [
        TableRow(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('Origin:'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(appData.plantTypes[plantTypeIndex].plants[plantIndex]
                  .plantData.facts.origin),
            ),
          ],
        ),
        TableRow(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('Names:'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(appData.plantTypes[plantTypeIndex].plants[plantIndex]
                  .plantData.facts.names),
            ),
          ],
        ),
        TableRow(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('Max Growth (approx):'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(appData.plantTypes[plantTypeIndex].plants[plantIndex]
                  .plantData.facts.maxGrowth),
            ),
          ],
        ),
        TableRow(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('Poisonous for pets:'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(appData.plantTypes[plantTypeIndex].plants[plantIndex]
                  .plantData.facts.poisonousForPets),
            ),
          ],
        ),
      ],
    );
  }
}
