import 'package:drop_cap_text/drop_cap_text.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:houseplant_guide/main.dart';

import '../../data/page/plant_page_data.dart';

class PlantCard extends StatelessWidget {
  const PlantCard(
      {required this.plantTypeIndex, required this.plantIndex, super.key});

  final int plantTypeIndex, plantIndex;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.push('/plant',
            extra: PlantPageData(
                title:
                    appData.plantTypes[plantTypeIndex].plants[plantIndex].title,
                plantTypeIndex: plantTypeIndex,
                plantIndex: plantIndex));
      },
      child: Container(
        margin: const EdgeInsets.all(20),
        decoration: BoxDecoration(
          border: Border.all(width: .2),
        ),
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Text(
              appData.plantTypes[plantTypeIndex].plants[plantIndex].title,
              style: Theme.of(context).textTheme.titleLarge,
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 10),
            DropCapText(
              appData.plantTypes[plantTypeIndex].plants[plantIndex].descr,
              dropCap: DropCap(
                  width: 100,
                  height: 100,
                  child: Image.asset(appData.plantTypes[plantTypeIndex]
                      .plants[plantIndex].imagePath)),
              dropCapPadding: const EdgeInsets.only(right: 20),
            ),
          ],
        ),
      ),
    );
  }
}
