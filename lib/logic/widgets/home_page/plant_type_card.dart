import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:houseplant_guide/logic/data/page/plant_type_page_data.dart';

class PlantTypeCard extends StatelessWidget {
  const PlantTypeCard(
      {required this.plantTypeName,
      required this.imagePath,
      required this.plantTypeIndex,
      super.key});

  final String plantTypeName, imagePath;
  final int plantTypeIndex;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.push('/plantType',
            extra:
                PlantTypePageData(title: plantTypeName, plantTypeIndex: plantTypeIndex));
      },
      child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage(imagePath))),
          // color: Colors.green,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                color: Colors.red.withOpacity(.7),
                child: Text(
                  plantTypeName,
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge
                      ?.copyWith(color: Colors.white),
                ),
              ),
            ],
          )),
    );
  }
}
