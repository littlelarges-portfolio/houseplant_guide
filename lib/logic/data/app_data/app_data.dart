import 'package:houseplant_guide/logic/data/plant/plant_care.dart';
import 'package:houseplant_guide/logic/data/plant/plant_data.dart';
import 'package:houseplant_guide/logic/data/plant/plant_facts.dart';
import 'package:houseplant_guide/logic/data/plant/plant_card_info.dart';
import 'package:houseplant_guide/logic/data/plant/plant_type_card_info.dart';

class AppData {
  List<PlantTypeCardInfo> plantTypes = [
    PlantTypeCardInfo(
        name: 'Foliage House Plants',
        imagePath: 'assets/images/plants/types/1_foliage_house_plants.jpg',
        plants: [
          PlantCardInfo(
            title: 'Aluminum PlantInfo – Pilea Cadierei',
            descr:
                'The aluminum plant is an easy-to-grow species native to China and Vietnam. It’s a bush-type plant that grows up to 12 inches tall and displays glossy green and silver oval-shaped leaves. Place the pilea cadierei in a brightly lit spot with sunlight to encourage it to thrive and grow well. Pruning each spring promotes new growth and spread.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/1_aluminum_plant_pilea_cadierei.jpg',
            plantData: PlantData(
                title: 'Aluminum PlantInfo',
                preDescr:
                    'The Aluminum plant is an easy-going house plant that is generally simple to please. So long as the Pilea cadierei plant gets the water and sunlight it wants it will continue to be a stunning addition to your indoor garden for years to come. \n\n Featuring wide bushy leaves that are marked with silver patterns, this native to China and Vietnam does wonderfully as a house plant.',
                descr:
                    'With the aluminum plants easy going nature and lack of any real growing problems, it will bush out from its container in leaves of silver and green for up to four years before it dies. The leaves are normally squat and broad, the body measuring up to 12 inches (30 cm) long and up to 8 inches (21 cm) wide. \n\n The root system is extensive and will break containers if it needs more room. This root system must be observed on an annual basis in order to re-pot before pot breakage occurs.',
                commonProblems: const [
                  'Bugs will occasionally eat the green parts of the leaves. Be alert for insects and remove them as needed.',
                  'Blight (plant disease) can occasionally be set up in a plant. It will usually begin in one leaf, turning it brown and rotten. Remove this stem from the plant to prevent it from spreading to the entire plant.',
                  'Leaves that have been exposed to too much direct sunlight will sunburn and turn yellow. Trim sunburned leaves off of the plant and move the plant to a shadier location immediately.'
                ],
                facts: PlantFacts(
                    origin: 'North Africa.',
                    names:
                        'Aluminum PlantInfo (common). Pilea Cadierei (botanical/scientific).',
                    maxGrowth: 'Height 1 – 2 ft.',
                    poisonousForPets: 'Toxic for cats and dogs.'),
                care: PlantCare(
                    temperature:
                        'Pilea cadierei requires a year-round temperature between 60-75 ºF (15-23 ºC). It can tolerate short periods of time outside of this temperature range, but continued exposure will kill the plant.',
                    light:
                        'This plant requires at least four hours of indirect, but bright, sunlight a day. Do not permit this plant to be exposed to overly bright or direct sunlight. Too much sun will burn the leaves and cause the green parts to turn brown.',
                    watering:
                        'The Aluminum plant needs to be watered differently during different times of the year. During spring and summer, the top quarter inch of the soil should be kept moist. During fall and winter, allow the top quarter inch of soil to dry out before watering again. Do not permit water to stand in the saucer underneath the plant.',
                    soil:
                        'This plant prefers a sandy soil mixture to live in. Combine one part clean sand (or perlite) with two parts peat to provide the required soil content for continued health.',
                    rePotting:
                        'Check the root density of Pilea cadierei every spring. If the roots are becoming overly dense, transplant the plant to a size larger pot. If you do not, the root system will break the container in an attempt to find more room to grow.',
                    humidity:
                        'Normal air humidity will please this plant. If your home is overly dry during the winter, mist the leaves every other day with plain water.',
                    propagation:
                        'This plant is propagated through cuttings. Take the cuttings in the early spring. Allow them to rest in a water source for one week, or until roots form. Then transplant to their own container of sandy soil mixture.')),
          ),
          PlantCardInfo(
            title: 'Cast Iron PlantInfo – Aspidistra Elatior',
            descr:
                'Cast iron is its name for one reason only; it’s cast iron in strength when adapting to low light and neglect. The only way to really upset this plant is by over-watering or re-potting too often. There are two varieties with plain green and variegated (cream-colored stripes) linear-shaped leaves. The elatior can grow up to nearly 1 meter tall, once it’s fully matured.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/2_cast_iron_plant_aspidistra_elatior.jpg',
            plantData: PlantData(
              title: 'Cast Iron PlantInfo',
              preDescr:
                  'The Cast Iron plant (Aspidistra Elatior) receives its common name for its ability to withstand neglect. A nice and easy to care for foliage house plant.\n\nEven the worst plant neglecter can keep the A. elatior alive and well with its tolerant attitude to light, dry air and lack of watering.\n\nThere are still a few conditions a grower has to provide and take care of, which are very easy. Over-watering and re-potting too often seem to be the main problems that can cause this species problems.',
              descr:
                  'A native to Japan and Taiwan the Aspidistra elatior is a hardy garden plant and very easy to grow ornamental plant for indoors. It has been known to bloom tiny flowers near the base of the foliage, although this seems to be a very rare occasion for growers – so its mainly grown for its foliage.\n\nHow it looks and leaves: There is a green foliage type and a variegated that displays cream colored stripes along the outer edge and/or the center of each leaf. The base of the leaves are rolled, then opens out and narrows in to a point. The linear leaves grow over a foot long and 5 inches wide – which display prominent ribs.\n\nEach of the leaves are grown from a stem that can be separated with its roots for growing new plants. You may also want to clean the leaves with a soft sponge (and water) to keep them looking attractive and dust free.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Japan & Taiwan.',
                  names:
                      'Cast iron plant (common). Aspidistra elatior (botanical/scientific).',
                  maxGrowth: 'Height 36 in (91 cm).',
                  poisonousForPets: 'Non-toxic to cats and dogs.'),
              care: PlantCare(
                  temperature:
                      'Average warmth is fine of approximately 60-75°F (15-24°C) and no lower than 45°F (8°C). They will tolerate much lower temperatures.',
                  light:
                      'A brightly lit room is good but not essential. They’re fine with low levels of light, but it’s best to keep them out of direct sunlight which will damage the leaves.',
                  watering:
                      'Water once the top one inch of soil becomes dry. Water less during winter and do not over water. Over watering to the degree that the soil becomes soggy for a period of time – can cause the plant to die. The Aspidistra elatior is drought tolerant so if it misses being watered even for a month it will look unwell but be right back with you once care and water is given again.',
                  soil:
                      'Most well draining potting mixes will suffice and good drainage holes at the bottom of the pot is advised.',
                  rePotting:
                      'Re-pot only when needed (spring) or once every 2 – 3 years.',
                  humidity:
                      'Average house humidity is advised but the cast iron plant can tolerate dry air conditions.',
                  propagation:
                      'Propagate by division when its re-potting time.'),
            ),
          ),
          PlantCardInfo(
            title: 'Chinese Evergreen – Aglaonema',
            descr:
                'An array of hybrid plants from the aglaonema genus cultivated over the years because of their increase in popularity. The Chinese evergreen grows up to 3ft tall and displays oval-shaped leaves that grow 30cm in length. A good supply of varieties offers growers different leaf color variations. Variegated does not tolerate low light as well as the plain green variety.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/3_chinese_evergreen_aglaonema.png',
            plantData: PlantData(
              title: 'Chinese Evergreen PlantInfo - Aglaonema',
              preDescr:
                  'The Chinese evergreen is the common name used for a collection of houseplants from the Aglaonema genus – which tend to tolerate low light conditions very well.\n\nBotanical name Aglaonemas will produce flowers (these are not very showy), but they’re grown primarily for attractive leathery leaves.',
              descr:
                  'There are many hybrid and cultivars of the Chinese evergreen plants available that have been cultivated over the last century. This is because of their increasing popularity for indoor growers to use them as ornamental houseplants for room decoration.\n\nThese slow-growing plant varieties include plain green, speckled, blotched, and variegated types. One of the most popular and sought-after is the Silver Queen. It has leaves covered in silver mainly with some small green patches.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Tropics and subtropics of Asia.',
                  names:
                      'Chinese evergreen (common). — A. commutatum, A modestum, A pictum, and others. (botanical/scientific).',
                  maxGrowth: 'Height 3ft.',
                  poisonousForPets: 'Toxic to cats, dogs, and horses.'),
              care: PlantCare(
                  temperature:
                      'Temperatures between 65-80 ºF (18-27 ºC) are ideal. Lower than 60ºF (15ºC) is not healthy for this plant and can cause dark patches on the leaves. If the leaves begin to curl and the edges turn brown the temperature is probably too low or cold drafts may be affecting the plant.',
                  light:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  watering:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  soil:
                      'A peat-based potting soil mixed with part perlite or sand to improve drainage is ideal medium or any other well-draining potting mix.',
                  rePotting:
                      'Re-pot once every two or three years during spring. They like to become slightly root bound – so don’t worry if they seem to show some roots through the bottom of the pot.',
                  humidity:
                      'Average to high humidity is needed. Increasing the humidity levels of a room “especially if the room has artificial heating” will improve the plant’s growth and prevent leaves from becoming dry and shrilling up. This can be done by spraying or a pebble tray.',
                  propagation:
                      'These are best propagated by dividing the root with a few stalks and leaves attached during the growing season of spring and summer. Stem cuttings may also be used.'),
            ),
          ),
        ]),
    PlantTypeCardInfo(
        name: 'Cactus House Plants',
        imagePath: 'assets/images/plants/types/2_cactus_house_plants.jpg',
        plants: [
          PlantCardInfo(
            title: 'Aluminum PlantInfo – Pilea Cadierei',
            descr:
                'The aluminum plant is an easy-to-grow species native to China and Vietnam. It’s a bush-type plant that grows up to 12 inches tall and displays glossy green and silver oval-shaped leaves. Place the pilea cadierei in a brightly lit spot with sunlight to encourage it to thrive and grow well. Pruning each spring promotes new growth and spread.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/1_aluminum_plant_pilea_cadierei.jpg',
            plantData: PlantData(
                title: 'Aluminum PlantInfo',
                preDescr:
                    'The Aluminum plant is an easy-going house plant that is generally simple to please. So long as the Pilea cadierei plant gets the water and sunlight it wants it will continue to be a stunning addition to your indoor garden for years to come. \n\n Featuring wide bushy leaves that are marked with silver patterns, this native to China and Vietnam does wonderfully as a house plant.',
                descr:
                    'With the aluminum plants easy going nature and lack of any real growing problems, it will bush out from its container in leaves of silver and green for up to four years before it dies. The leaves are normally squat and broad, the body measuring up to 12 inches (30 cm) long and up to 8 inches (21 cm) wide. \n\n The root system is extensive and will break containers if it needs more room. This root system must be observed on an annual basis in order to re-pot before pot breakage occurs.',
                commonProblems: const [
                  'Bugs will occasionally eat the green parts of the leaves. Be alert for insects and remove them as needed.',
                  'Blight (plant disease) can occasionally be set up in a plant. It will usually begin in one leaf, turning it brown and rotten. Remove this stem from the plant to prevent it from spreading to the entire plant.',
                  'Leaves that have been exposed to too much direct sunlight will sunburn and turn yellow. Trim sunburned leaves off of the plant and move the plant to a shadier location immediately.'
                ],
                facts: PlantFacts(
                    origin: 'North Africa.',
                    names:
                        'Aluminum PlantInfo (common). Pilea Cadierei (botanical/scientific).',
                    maxGrowth: 'Height 1 – 2 ft.',
                    poisonousForPets: 'Toxic for cats and dogs.'),
                care: PlantCare(
                    temperature:
                        'Pilea cadierei requires a year-round temperature between 60-75 ºF (15-23 ºC). It can tolerate short periods of time outside of this temperature range, but continued exposure will kill the plant.',
                    light:
                        'This plant requires at least four hours of indirect, but bright, sunlight a day. Do not permit this plant to be exposed to overly bright or direct sunlight. Too much sun will burn the leaves and cause the green parts to turn brown.',
                    watering:
                        'The Aluminum plant needs to be watered differently during different times of the year. During spring and summer, the top quarter inch of the soil should be kept moist. During fall and winter, allow the top quarter inch of soil to dry out before watering again. Do not permit water to stand in the saucer underneath the plant.',
                    soil:
                        'This plant prefers a sandy soil mixture to live in. Combine one part clean sand (or perlite) with two parts peat to provide the required soil content for continued health.',
                    rePotting:
                        'Check the root density of Pilea cadierei every spring. If the roots are becoming overly dense, transplant the plant to a size larger pot. If you do not, the root system will break the container in an attempt to find more room to grow.',
                    humidity:
                        'Normal air humidity will please this plant. If your home is overly dry during the winter, mist the leaves every other day with plain water.',
                    propagation:
                        'This plant is propagated through cuttings. Take the cuttings in the early spring. Allow them to rest in a water source for one week, or until roots form. Then transplant to their own container of sandy soil mixture.')),
          ),
          PlantCardInfo(
            title: 'Cast Iron PlantInfo – Aspidistra Elatior',
            descr:
                'Cast iron is its name for one reason only; it’s cast iron in strength when adapting to low light and neglect. The only way to really upset this plant is by over-watering or re-potting too often. There are two varieties with plain green and variegated (cream-colored stripes) linear-shaped leaves. The elatior can grow up to nearly 1 meter tall, once it’s fully matured.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/2_cast_iron_plant_aspidistra_elatior.jpg',
            plantData: PlantData(
              title: 'Cast Iron PlantInfo',
              preDescr:
                  'The Cast Iron plant (Aspidistra Elatior) receives its common name for its ability to withstand neglect. A nice and easy to care for foliage house plant.\n\nEven the worst plant neglecter can keep the A. elatior alive and well with its tolerant attitude to light, dry air and lack of watering.\n\nThere are still a few conditions a grower has to provide and take care of, which are very easy. Over-watering and re-potting too often seem to be the main problems that can cause this species problems.',
              descr:
                  'A native to Japan and Taiwan the Aspidistra elatior is a hardy garden plant and very easy to grow ornamental plant for indoors. It has been known to bloom tiny flowers near the base of the foliage, although this seems to be a very rare occasion for growers – so its mainly grown for its foliage.\n\nHow it looks and leaves: There is a green foliage type and a variegated that displays cream colored stripes along the outer edge and/or the center of each leaf. The base of the leaves are rolled, then opens out and narrows in to a point. The linear leaves grow over a foot long and 5 inches wide – which display prominent ribs.\n\nEach of the leaves are grown from a stem that can be separated with its roots for growing new plants. You may also want to clean the leaves with a soft sponge (and water) to keep them looking attractive and dust free.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Japan & Taiwan.',
                  names:
                      'Cast iron plant (common). Aspidistra elatior (botanical/scientific).',
                  maxGrowth: 'Height 36 in (91 cm).',
                  poisonousForPets: 'Non-toxic to cats and dogs.'),
              care: PlantCare(
                  temperature:
                      'Average warmth is fine of approximately 60-75°F (15-24°C) and no lower than 45°F (8°C). They will tolerate much lower temperatures.',
                  light:
                      'A brightly lit room is good but not essential. They’re fine with low levels of light, but it’s best to keep them out of direct sunlight which will damage the leaves.',
                  watering:
                      'Water once the top one inch of soil becomes dry. Water less during winter and do not over water. Over watering to the degree that the soil becomes soggy for a period of time – can cause the plant to die. The Aspidistra elatior is drought tolerant so if it misses being watered even for a month it will look unwell but be right back with you once care and water is given again.',
                  soil:
                      'Most well draining potting mixes will suffice and good drainage holes at the bottom of the pot is advised.',
                  rePotting:
                      'Re-pot only when needed (spring) or once every 2 – 3 years.',
                  humidity:
                      'Average house humidity is advised but the cast iron plant can tolerate dry air conditions.',
                  propagation:
                      'Propagate by division when its re-potting time.'),
            ),
          ),
          PlantCardInfo(
            title: 'Chinese Evergreen – Aglaonema',
            descr:
                'An array of hybrid plants from the aglaonema genus cultivated over the years because of their increase in popularity. The Chinese evergreen grows up to 3ft tall and displays oval-shaped leaves that grow 30cm in length. A good supply of varieties offers growers different leaf color variations. Variegated does not tolerate low light as well as the plain green variety.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/3_chinese_evergreen_aglaonema.png',
            plantData: PlantData(
              title: 'Chinese Evergreen PlantInfo - Aglaonema',
              preDescr:
                  'The Chinese evergreen is the common name used for a collection of houseplants from the Aglaonema genus – which tend to tolerate low light conditions very well.\n\nBotanical name Aglaonemas will produce flowers (these are not very showy), but they’re grown primarily for attractive leathery leaves.',
              descr:
                  'There are many hybrid and cultivars of the Chinese evergreen plants available that have been cultivated over the last century. This is because of their increasing popularity for indoor growers to use them as ornamental houseplants for room decoration.\n\nThese slow-growing plant varieties include plain green, speckled, blotched, and variegated types. One of the most popular and sought-after is the Silver Queen. It has leaves covered in silver mainly with some small green patches.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Tropics and subtropics of Asia.',
                  names:
                      'Chinese evergreen (common). — A. commutatum, A modestum, A pictum, and others. (botanical/scientific).',
                  maxGrowth: 'Height 3ft.',
                  poisonousForPets: 'Toxic to cats, dogs, and horses.'),
              care: PlantCare(
                  temperature:
                      'Temperatures between 65-80 ºF (18-27 ºC) are ideal. Lower than 60ºF (15ºC) is not healthy for this plant and can cause dark patches on the leaves. If the leaves begin to curl and the edges turn brown the temperature is probably too low or cold drafts may be affecting the plant.',
                  light:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  watering:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  soil:
                      'A peat-based potting soil mixed with part perlite or sand to improve drainage is ideal medium or any other well-draining potting mix.',
                  rePotting:
                      'Re-pot once every two or three years during spring. They like to become slightly root bound – so don’t worry if they seem to show some roots through the bottom of the pot.',
                  humidity:
                      'Average to high humidity is needed. Increasing the humidity levels of a room “especially if the room has artificial heating” will improve the plant’s growth and prevent leaves from becoming dry and shrilling up. This can be done by spraying or a pebble tray.',
                  propagation:
                      'These are best propagated by dividing the root with a few stalks and leaves attached during the growing season of spring and summer. Stem cuttings may also be used.'),
            ),
          ),
        ]),
    PlantTypeCardInfo(
        name: 'Indoor Palm Plants',
        imagePath: 'assets/images/plants/types/3_indoor_palm_plants.jpg',
        plants: [
          PlantCardInfo(
            title: 'Aluminum PlantInfo – Pilea Cadierei',
            descr:
                'The aluminum plant is an easy-to-grow species native to China and Vietnam. It’s a bush-type plant that grows up to 12 inches tall and displays glossy green and silver oval-shaped leaves. Place the pilea cadierei in a brightly lit spot with sunlight to encourage it to thrive and grow well. Pruning each spring promotes new growth and spread.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/1_aluminum_plant_pilea_cadierei.jpg',
            plantData: PlantData(
                title: 'Aluminum PlantInfo',
                preDescr:
                    'The Aluminum plant is an easy-going house plant that is generally simple to please. So long as the Pilea cadierei plant gets the water and sunlight it wants it will continue to be a stunning addition to your indoor garden for years to come. \n\n Featuring wide bushy leaves that are marked with silver patterns, this native to China and Vietnam does wonderfully as a house plant.',
                descr:
                    'With the aluminum plants easy going nature and lack of any real growing problems, it will bush out from its container in leaves of silver and green for up to four years before it dies. The leaves are normally squat and broad, the body measuring up to 12 inches (30 cm) long and up to 8 inches (21 cm) wide. \n\n The root system is extensive and will break containers if it needs more room. This root system must be observed on an annual basis in order to re-pot before pot breakage occurs.',
                commonProblems: const [
                  'Bugs will occasionally eat the green parts of the leaves. Be alert for insects and remove them as needed.',
                  'Blight (plant disease) can occasionally be set up in a plant. It will usually begin in one leaf, turning it brown and rotten. Remove this stem from the plant to prevent it from spreading to the entire plant.',
                  'Leaves that have been exposed to too much direct sunlight will sunburn and turn yellow. Trim sunburned leaves off of the plant and move the plant to a shadier location immediately.'
                ],
                facts: PlantFacts(
                    origin: 'North Africa.',
                    names:
                        'Aluminum PlantInfo (common). Pilea Cadierei (botanical/scientific).',
                    maxGrowth: 'Height 1 – 2 ft.',
                    poisonousForPets: 'Toxic for cats and dogs.'),
                care: PlantCare(
                    temperature:
                        'Pilea cadierei requires a year-round temperature between 60-75 ºF (15-23 ºC). It can tolerate short periods of time outside of this temperature range, but continued exposure will kill the plant.',
                    light:
                        'This plant requires at least four hours of indirect, but bright, sunlight a day. Do not permit this plant to be exposed to overly bright or direct sunlight. Too much sun will burn the leaves and cause the green parts to turn brown.',
                    watering:
                        'The Aluminum plant needs to be watered differently during different times of the year. During spring and summer, the top quarter inch of the soil should be kept moist. During fall and winter, allow the top quarter inch of soil to dry out before watering again. Do not permit water to stand in the saucer underneath the plant.',
                    soil:
                        'This plant prefers a sandy soil mixture to live in. Combine one part clean sand (or perlite) with two parts peat to provide the required soil content for continued health.',
                    rePotting:
                        'Check the root density of Pilea cadierei every spring. If the roots are becoming overly dense, transplant the plant to a size larger pot. If you do not, the root system will break the container in an attempt to find more room to grow.',
                    humidity:
                        'Normal air humidity will please this plant. If your home is overly dry during the winter, mist the leaves every other day with plain water.',
                    propagation:
                        'This plant is propagated through cuttings. Take the cuttings in the early spring. Allow them to rest in a water source for one week, or until roots form. Then transplant to their own container of sandy soil mixture.')),
          ),
          PlantCardInfo(
            title: 'Cast Iron PlantInfo – Aspidistra Elatior',
            descr:
                'Cast iron is its name for one reason only; it’s cast iron in strength when adapting to low light and neglect. The only way to really upset this plant is by over-watering or re-potting too often. There are two varieties with plain green and variegated (cream-colored stripes) linear-shaped leaves. The elatior can grow up to nearly 1 meter tall, once it’s fully matured.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/2_cast_iron_plant_aspidistra_elatior.jpg',
            plantData: PlantData(
              title: 'Cast Iron PlantInfo',
              preDescr:
                  'The Cast Iron plant (Aspidistra Elatior) receives its common name for its ability to withstand neglect. A nice and easy to care for foliage house plant.\n\nEven the worst plant neglecter can keep the A. elatior alive and well with its tolerant attitude to light, dry air and lack of watering.\n\nThere are still a few conditions a grower has to provide and take care of, which are very easy. Over-watering and re-potting too often seem to be the main problems that can cause this species problems.',
              descr:
                  'A native to Japan and Taiwan the Aspidistra elatior is a hardy garden plant and very easy to grow ornamental plant for indoors. It has been known to bloom tiny flowers near the base of the foliage, although this seems to be a very rare occasion for growers – so its mainly grown for its foliage.\n\nHow it looks and leaves: There is a green foliage type and a variegated that displays cream colored stripes along the outer edge and/or the center of each leaf. The base of the leaves are rolled, then opens out and narrows in to a point. The linear leaves grow over a foot long and 5 inches wide – which display prominent ribs.\n\nEach of the leaves are grown from a stem that can be separated with its roots for growing new plants. You may also want to clean the leaves with a soft sponge (and water) to keep them looking attractive and dust free.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Japan & Taiwan.',
                  names:
                      'Cast iron plant (common). Aspidistra elatior (botanical/scientific).',
                  maxGrowth: 'Height 36 in (91 cm).',
                  poisonousForPets: 'Non-toxic to cats and dogs.'),
              care: PlantCare(
                  temperature:
                      'Average warmth is fine of approximately 60-75°F (15-24°C) and no lower than 45°F (8°C). They will tolerate much lower temperatures.',
                  light:
                      'A brightly lit room is good but not essential. They’re fine with low levels of light, but it’s best to keep them out of direct sunlight which will damage the leaves.',
                  watering:
                      'Water once the top one inch of soil becomes dry. Water less during winter and do not over water. Over watering to the degree that the soil becomes soggy for a period of time – can cause the plant to die. The Aspidistra elatior is drought tolerant so if it misses being watered even for a month it will look unwell but be right back with you once care and water is given again.',
                  soil:
                      'Most well draining potting mixes will suffice and good drainage holes at the bottom of the pot is advised.',
                  rePotting:
                      'Re-pot only when needed (spring) or once every 2 – 3 years.',
                  humidity:
                      'Average house humidity is advised but the cast iron plant can tolerate dry air conditions.',
                  propagation:
                      'Propagate by division when its re-potting time.'),
            ),
          ),
          PlantCardInfo(
            title: 'Chinese Evergreen – Aglaonema',
            descr:
                'An array of hybrid plants from the aglaonema genus cultivated over the years because of their increase in popularity. The Chinese evergreen grows up to 3ft tall and displays oval-shaped leaves that grow 30cm in length. A good supply of varieties offers growers different leaf color variations. Variegated does not tolerate low light as well as the plain green variety.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/3_chinese_evergreen_aglaonema.png',
            plantData: PlantData(
              title: 'Chinese Evergreen PlantInfo - Aglaonema',
              preDescr:
                  'The Chinese evergreen is the common name used for a collection of houseplants from the Aglaonema genus – which tend to tolerate low light conditions very well.\n\nBotanical name Aglaonemas will produce flowers (these are not very showy), but they’re grown primarily for attractive leathery leaves.',
              descr:
                  'There are many hybrid and cultivars of the Chinese evergreen plants available that have been cultivated over the last century. This is because of their increasing popularity for indoor growers to use them as ornamental houseplants for room decoration.\n\nThese slow-growing plant varieties include plain green, speckled, blotched, and variegated types. One of the most popular and sought-after is the Silver Queen. It has leaves covered in silver mainly with some small green patches.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Tropics and subtropics of Asia.',
                  names:
                      'Chinese evergreen (common). — A. commutatum, A modestum, A pictum, and others. (botanical/scientific).',
                  maxGrowth: 'Height 3ft.',
                  poisonousForPets: 'Toxic to cats, dogs, and horses.'),
              care: PlantCare(
                  temperature:
                      'Temperatures between 65-80 ºF (18-27 ºC) are ideal. Lower than 60ºF (15ºC) is not healthy for this plant and can cause dark patches on the leaves. If the leaves begin to curl and the edges turn brown the temperature is probably too low or cold drafts may be affecting the plant.',
                  light:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  watering:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  soil:
                      'A peat-based potting soil mixed with part perlite or sand to improve drainage is ideal medium or any other well-draining potting mix.',
                  rePotting:
                      'Re-pot once every two or three years during spring. They like to become slightly root bound – so don’t worry if they seem to show some roots through the bottom of the pot.',
                  humidity:
                      'Average to high humidity is needed. Increasing the humidity levels of a room “especially if the room has artificial heating” will improve the plant’s growth and prevent leaves from becoming dry and shrilling up. This can be done by spraying or a pebble tray.',
                  propagation:
                      'These are best propagated by dividing the root with a few stalks and leaves attached during the growing season of spring and summer. Stem cuttings may also be used.'),
            ),
          ),
        ]),
    PlantTypeCardInfo(
        name: 'Indoor Hanging Basket Plants',
        imagePath:
            'assets/images/plants/types/4_indoor_hanging_basket_plants.jpg',
        plants: [
          PlantCardInfo(
            title: 'Aluminum PlantInfo – Pilea Cadierei',
            descr:
                'The aluminum plant is an easy-to-grow species native to China and Vietnam. It’s a bush-type plant that grows up to 12 inches tall and displays glossy green and silver oval-shaped leaves. Place the pilea cadierei in a brightly lit spot with sunlight to encourage it to thrive and grow well. Pruning each spring promotes new growth and spread.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/1_aluminum_plant_pilea_cadierei.jpg',
            plantData: PlantData(
                title: 'Aluminum PlantInfo',
                preDescr:
                    'The Aluminum plant is an easy-going house plant that is generally simple to please. So long as the Pilea cadierei plant gets the water and sunlight it wants it will continue to be a stunning addition to your indoor garden for years to come. \n\n Featuring wide bushy leaves that are marked with silver patterns, this native to China and Vietnam does wonderfully as a house plant.',
                descr:
                    'With the aluminum plants easy going nature and lack of any real growing problems, it will bush out from its container in leaves of silver and green for up to four years before it dies. The leaves are normally squat and broad, the body measuring up to 12 inches (30 cm) long and up to 8 inches (21 cm) wide. \n\n The root system is extensive and will break containers if it needs more room. This root system must be observed on an annual basis in order to re-pot before pot breakage occurs.',
                commonProblems: const [
                  'Bugs will occasionally eat the green parts of the leaves. Be alert for insects and remove them as needed.',
                  'Blight (plant disease) can occasionally be set up in a plant. It will usually begin in one leaf, turning it brown and rotten. Remove this stem from the plant to prevent it from spreading to the entire plant.',
                  'Leaves that have been exposed to too much direct sunlight will sunburn and turn yellow. Trim sunburned leaves off of the plant and move the plant to a shadier location immediately.'
                ],
                facts: PlantFacts(
                    origin: 'North Africa.',
                    names:
                        'Aluminum PlantInfo (common). Pilea Cadierei (botanical/scientific).',
                    maxGrowth: 'Height 1 – 2 ft.',
                    poisonousForPets: 'Toxic for cats and dogs.'),
                care: PlantCare(
                    temperature:
                        'Pilea cadierei requires a year-round temperature between 60-75 ºF (15-23 ºC). It can tolerate short periods of time outside of this temperature range, but continued exposure will kill the plant.',
                    light:
                        'This plant requires at least four hours of indirect, but bright, sunlight a day. Do not permit this plant to be exposed to overly bright or direct sunlight. Too much sun will burn the leaves and cause the green parts to turn brown.',
                    watering:
                        'The Aluminum plant needs to be watered differently during different times of the year. During spring and summer, the top quarter inch of the soil should be kept moist. During fall and winter, allow the top quarter inch of soil to dry out before watering again. Do not permit water to stand in the saucer underneath the plant.',
                    soil:
                        'This plant prefers a sandy soil mixture to live in. Combine one part clean sand (or perlite) with two parts peat to provide the required soil content for continued health.',
                    rePotting:
                        'Check the root density of Pilea cadierei every spring. If the roots are becoming overly dense, transplant the plant to a size larger pot. If you do not, the root system will break the container in an attempt to find more room to grow.',
                    humidity:
                        'Normal air humidity will please this plant. If your home is overly dry during the winter, mist the leaves every other day with plain water.',
                    propagation:
                        'This plant is propagated through cuttings. Take the cuttings in the early spring. Allow them to rest in a water source for one week, or until roots form. Then transplant to their own container of sandy soil mixture.')),
          ),
          PlantCardInfo(
            title: 'Cast Iron PlantInfo – Aspidistra Elatior',
            descr:
                'Cast iron is its name for one reason only; it’s cast iron in strength when adapting to low light and neglect. The only way to really upset this plant is by over-watering or re-potting too often. There are two varieties with plain green and variegated (cream-colored stripes) linear-shaped leaves. The elatior can grow up to nearly 1 meter tall, once it’s fully matured.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/2_cast_iron_plant_aspidistra_elatior.jpg',
            plantData: PlantData(
              title: 'Cast Iron PlantInfo',
              preDescr:
                  'The Cast Iron plant (Aspidistra Elatior) receives its common name for its ability to withstand neglect. A nice and easy to care for foliage house plant.\n\nEven the worst plant neglecter can keep the A. elatior alive and well with its tolerant attitude to light, dry air and lack of watering.\n\nThere are still a few conditions a grower has to provide and take care of, which are very easy. Over-watering and re-potting too often seem to be the main problems that can cause this species problems.',
              descr:
                  'A native to Japan and Taiwan the Aspidistra elatior is a hardy garden plant and very easy to grow ornamental plant for indoors. It has been known to bloom tiny flowers near the base of the foliage, although this seems to be a very rare occasion for growers – so its mainly grown for its foliage.\n\nHow it looks and leaves: There is a green foliage type and a variegated that displays cream colored stripes along the outer edge and/or the center of each leaf. The base of the leaves are rolled, then opens out and narrows in to a point. The linear leaves grow over a foot long and 5 inches wide – which display prominent ribs.\n\nEach of the leaves are grown from a stem that can be separated with its roots for growing new plants. You may also want to clean the leaves with a soft sponge (and water) to keep them looking attractive and dust free.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Japan & Taiwan.',
                  names:
                      'Cast iron plant (common). Aspidistra elatior (botanical/scientific).',
                  maxGrowth: 'Height 36 in (91 cm).',
                  poisonousForPets: 'Non-toxic to cats and dogs.'),
              care: PlantCare(
                  temperature:
                      'Average warmth is fine of approximately 60-75°F (15-24°C) and no lower than 45°F (8°C). They will tolerate much lower temperatures.',
                  light:
                      'A brightly lit room is good but not essential. They’re fine with low levels of light, but it’s best to keep them out of direct sunlight which will damage the leaves.',
                  watering:
                      'Water once the top one inch of soil becomes dry. Water less during winter and do not over water. Over watering to the degree that the soil becomes soggy for a period of time – can cause the plant to die. The Aspidistra elatior is drought tolerant so if it misses being watered even for a month it will look unwell but be right back with you once care and water is given again.',
                  soil:
                      'Most well draining potting mixes will suffice and good drainage holes at the bottom of the pot is advised.',
                  rePotting:
                      'Re-pot only when needed (spring) or once every 2 – 3 years.',
                  humidity:
                      'Average house humidity is advised but the cast iron plant can tolerate dry air conditions.',
                  propagation:
                      'Propagate by division when its re-potting time.'),
            ),
          ),
          PlantCardInfo(
            title: 'Chinese Evergreen – Aglaonema',
            descr:
                'An array of hybrid plants from the aglaonema genus cultivated over the years because of their increase in popularity. The Chinese evergreen grows up to 3ft tall and displays oval-shaped leaves that grow 30cm in length. A good supply of varieties offers growers different leaf color variations. Variegated does not tolerate low light as well as the plain green variety.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/3_chinese_evergreen_aglaonema.png',
            plantData: PlantData(
              title: 'Chinese Evergreen PlantInfo - Aglaonema',
              preDescr:
                  'The Chinese evergreen is the common name used for a collection of houseplants from the Aglaonema genus – which tend to tolerate low light conditions very well.\n\nBotanical name Aglaonemas will produce flowers (these are not very showy), but they’re grown primarily for attractive leathery leaves.',
              descr:
                  'There are many hybrid and cultivars of the Chinese evergreen plants available that have been cultivated over the last century. This is because of their increasing popularity for indoor growers to use them as ornamental houseplants for room decoration.\n\nThese slow-growing plant varieties include plain green, speckled, blotched, and variegated types. One of the most popular and sought-after is the Silver Queen. It has leaves covered in silver mainly with some small green patches.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Tropics and subtropics of Asia.',
                  names:
                      'Chinese evergreen (common). — A. commutatum, A modestum, A pictum, and others. (botanical/scientific).',
                  maxGrowth: 'Height 3ft.',
                  poisonousForPets: 'Toxic to cats, dogs, and horses.'),
              care: PlantCare(
                  temperature:
                      'Temperatures between 65-80 ºF (18-27 ºC) are ideal. Lower than 60ºF (15ºC) is not healthy for this plant and can cause dark patches on the leaves. If the leaves begin to curl and the edges turn brown the temperature is probably too low or cold drafts may be affecting the plant.',
                  light:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  watering:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  soil:
                      'A peat-based potting soil mixed with part perlite or sand to improve drainage is ideal medium or any other well-draining potting mix.',
                  rePotting:
                      'Re-pot once every two or three years during spring. They like to become slightly root bound – so don’t worry if they seem to show some roots through the bottom of the pot.',
                  humidity:
                      'Average to high humidity is needed. Increasing the humidity levels of a room “especially if the room has artificial heating” will improve the plant’s growth and prevent leaves from becoming dry and shrilling up. This can be done by spraying or a pebble tray.',
                  propagation:
                      'These are best propagated by dividing the root with a few stalks and leaves attached during the growing season of spring and summer. Stem cuttings may also be used.'),
            ),
          ),
        ]),
    PlantTypeCardInfo(
        name: 'Climbers and Trailing Plants',
        imagePath:
            'assets/images/plants/types/5_climbers_and_trailing_plants.jpg',
        plants: [
          PlantCardInfo(
            title: 'Aluminum PlantInfo – Pilea Cadierei',
            descr:
                'The aluminum plant is an easy-to-grow species native to China and Vietnam. It’s a bush-type plant that grows up to 12 inches tall and displays glossy green and silver oval-shaped leaves. Place the pilea cadierei in a brightly lit spot with sunlight to encourage it to thrive and grow well. Pruning each spring promotes new growth and spread.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/1_aluminum_plant_pilea_cadierei.jpg',
            plantData: PlantData(
                title: 'Aluminum PlantInfo',
                preDescr:
                    'The Aluminum plant is an easy-going house plant that is generally simple to please. So long as the Pilea cadierei plant gets the water and sunlight it wants it will continue to be a stunning addition to your indoor garden for years to come. \n\n Featuring wide bushy leaves that are marked with silver patterns, this native to China and Vietnam does wonderfully as a house plant.',
                descr:
                    'With the aluminum plants easy going nature and lack of any real growing problems, it will bush out from its container in leaves of silver and green for up to four years before it dies. The leaves are normally squat and broad, the body measuring up to 12 inches (30 cm) long and up to 8 inches (21 cm) wide. \n\n The root system is extensive and will break containers if it needs more room. This root system must be observed on an annual basis in order to re-pot before pot breakage occurs.',
                commonProblems: const [
                  'Bugs will occasionally eat the green parts of the leaves. Be alert for insects and remove them as needed.',
                  'Blight (plant disease) can occasionally be set up in a plant. It will usually begin in one leaf, turning it brown and rotten. Remove this stem from the plant to prevent it from spreading to the entire plant.',
                  'Leaves that have been exposed to too much direct sunlight will sunburn and turn yellow. Trim sunburned leaves off of the plant and move the plant to a shadier location immediately.'
                ],
                facts: PlantFacts(
                    origin: 'North Africa.',
                    names:
                        'Aluminum PlantInfo (common). Pilea Cadierei (botanical/scientific).',
                    maxGrowth: 'Height 1 – 2 ft.',
                    poisonousForPets: 'Toxic for cats and dogs.'),
                care: PlantCare(
                    temperature:
                        'Pilea cadierei requires a year-round temperature between 60-75 ºF (15-23 ºC). It can tolerate short periods of time outside of this temperature range, but continued exposure will kill the plant.',
                    light:
                        'This plant requires at least four hours of indirect, but bright, sunlight a day. Do not permit this plant to be exposed to overly bright or direct sunlight. Too much sun will burn the leaves and cause the green parts to turn brown.',
                    watering:
                        'The Aluminum plant needs to be watered differently during different times of the year. During spring and summer, the top quarter inch of the soil should be kept moist. During fall and winter, allow the top quarter inch of soil to dry out before watering again. Do not permit water to stand in the saucer underneath the plant.',
                    soil:
                        'This plant prefers a sandy soil mixture to live in. Combine one part clean sand (or perlite) with two parts peat to provide the required soil content for continued health.',
                    rePotting:
                        'Check the root density of Pilea cadierei every spring. If the roots are becoming overly dense, transplant the plant to a size larger pot. If you do not, the root system will break the container in an attempt to find more room to grow.',
                    humidity:
                        'Normal air humidity will please this plant. If your home is overly dry during the winter, mist the leaves every other day with plain water.',
                    propagation:
                        'This plant is propagated through cuttings. Take the cuttings in the early spring. Allow them to rest in a water source for one week, or until roots form. Then transplant to their own container of sandy soil mixture.')),
          ),
          PlantCardInfo(
            title: 'Cast Iron PlantInfo – Aspidistra Elatior',
            descr:
                'Cast iron is its name for one reason only; it’s cast iron in strength when adapting to low light and neglect. The only way to really upset this plant is by over-watering or re-potting too often. There are two varieties with plain green and variegated (cream-colored stripes) linear-shaped leaves. The elatior can grow up to nearly 1 meter tall, once it’s fully matured.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/2_cast_iron_plant_aspidistra_elatior.jpg',
            plantData: PlantData(
              title: 'Cast Iron PlantInfo',
              preDescr:
                  'The Cast Iron plant (Aspidistra Elatior) receives its common name for its ability to withstand neglect. A nice and easy to care for foliage house plant.\n\nEven the worst plant neglecter can keep the A. elatior alive and well with its tolerant attitude to light, dry air and lack of watering.\n\nThere are still a few conditions a grower has to provide and take care of, which are very easy. Over-watering and re-potting too often seem to be the main problems that can cause this species problems.',
              descr:
                  'A native to Japan and Taiwan the Aspidistra elatior is a hardy garden plant and very easy to grow ornamental plant for indoors. It has been known to bloom tiny flowers near the base of the foliage, although this seems to be a very rare occasion for growers – so its mainly grown for its foliage.\n\nHow it looks and leaves: There is a green foliage type and a variegated that displays cream colored stripes along the outer edge and/or the center of each leaf. The base of the leaves are rolled, then opens out and narrows in to a point. The linear leaves grow over a foot long and 5 inches wide – which display prominent ribs.\n\nEach of the leaves are grown from a stem that can be separated with its roots for growing new plants. You may also want to clean the leaves with a soft sponge (and water) to keep them looking attractive and dust free.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Japan & Taiwan.',
                  names:
                      'Cast iron plant (common). Aspidistra elatior (botanical/scientific).',
                  maxGrowth: 'Height 36 in (91 cm).',
                  poisonousForPets: 'Non-toxic to cats and dogs.'),
              care: PlantCare(
                  temperature:
                      'Average warmth is fine of approximately 60-75°F (15-24°C) and no lower than 45°F (8°C). They will tolerate much lower temperatures.',
                  light:
                      'A brightly lit room is good but not essential. They’re fine with low levels of light, but it’s best to keep them out of direct sunlight which will damage the leaves.',
                  watering:
                      'Water once the top one inch of soil becomes dry. Water less during winter and do not over water. Over watering to the degree that the soil becomes soggy for a period of time – can cause the plant to die. The Aspidistra elatior is drought tolerant so if it misses being watered even for a month it will look unwell but be right back with you once care and water is given again.',
                  soil:
                      'Most well draining potting mixes will suffice and good drainage holes at the bottom of the pot is advised.',
                  rePotting:
                      'Re-pot only when needed (spring) or once every 2 – 3 years.',
                  humidity:
                      'Average house humidity is advised but the cast iron plant can tolerate dry air conditions.',
                  propagation:
                      'Propagate by division when its re-potting time.'),
            ),
          ),
          PlantCardInfo(
            title: 'Chinese Evergreen – Aglaonema',
            descr:
                'An array of hybrid plants from the aglaonema genus cultivated over the years because of their increase in popularity. The Chinese evergreen grows up to 3ft tall and displays oval-shaped leaves that grow 30cm in length. A good supply of varieties offers growers different leaf color variations. Variegated does not tolerate low light as well as the plain green variety.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/3_chinese_evergreen_aglaonema.png',
            plantData: PlantData(
              title: 'Chinese Evergreen PlantInfo - Aglaonema',
              preDescr:
                  'The Chinese evergreen is the common name used for a collection of houseplants from the Aglaonema genus – which tend to tolerate low light conditions very well.\n\nBotanical name Aglaonemas will produce flowers (these are not very showy), but they’re grown primarily for attractive leathery leaves.',
              descr:
                  'There are many hybrid and cultivars of the Chinese evergreen plants available that have been cultivated over the last century. This is because of their increasing popularity for indoor growers to use them as ornamental houseplants for room decoration.\n\nThese slow-growing plant varieties include plain green, speckled, blotched, and variegated types. One of the most popular and sought-after is the Silver Queen. It has leaves covered in silver mainly with some small green patches.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Tropics and subtropics of Asia.',
                  names:
                      'Chinese evergreen (common). — A. commutatum, A modestum, A pictum, and others. (botanical/scientific).',
                  maxGrowth: 'Height 3ft.',
                  poisonousForPets: 'Toxic to cats, dogs, and horses.'),
              care: PlantCare(
                  temperature:
                      'Temperatures between 65-80 ºF (18-27 ºC) are ideal. Lower than 60ºF (15ºC) is not healthy for this plant and can cause dark patches on the leaves. If the leaves begin to curl and the edges turn brown the temperature is probably too low or cold drafts may be affecting the plant.',
                  light:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  watering:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  soil:
                      'A peat-based potting soil mixed with part perlite or sand to improve drainage is ideal medium or any other well-draining potting mix.',
                  rePotting:
                      'Re-pot once every two or three years during spring. They like to become slightly root bound – so don’t worry if they seem to show some roots through the bottom of the pot.',
                  humidity:
                      'Average to high humidity is needed. Increasing the humidity levels of a room “especially if the room has artificial heating” will improve the plant’s growth and prevent leaves from becoming dry and shrilling up. This can be done by spraying or a pebble tray.',
                  propagation:
                      'These are best propagated by dividing the root with a few stalks and leaves attached during the growing season of spring and summer. Stem cuttings may also be used.'),
            ),
          ),
        ]),
    PlantTypeCardInfo(
        name: 'Bulb Type House Plants',
        imagePath: 'assets/images/plants/types/6_bulb_type_house_plants.jpg',
        plants: [
          PlantCardInfo(
            title: 'Aluminum PlantInfo – Pilea Cadierei',
            descr:
                'The aluminum plant is an easy-to-grow species native to China and Vietnam. It’s a bush-type plant that grows up to 12 inches tall and displays glossy green and silver oval-shaped leaves. Place the pilea cadierei in a brightly lit spot with sunlight to encourage it to thrive and grow well. Pruning each spring promotes new growth and spread.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/1_aluminum_plant_pilea_cadierei.jpg',
            plantData: PlantData(
                title: 'Aluminum PlantInfo',
                preDescr:
                    'The Aluminum plant is an easy-going house plant that is generally simple to please. So long as the Pilea cadierei plant gets the water and sunlight it wants it will continue to be a stunning addition to your indoor garden for years to come. \n\n Featuring wide bushy leaves that are marked with silver patterns, this native to China and Vietnam does wonderfully as a house plant.',
                descr:
                    'With the aluminum plants easy going nature and lack of any real growing problems, it will bush out from its container in leaves of silver and green for up to four years before it dies. The leaves are normally squat and broad, the body measuring up to 12 inches (30 cm) long and up to 8 inches (21 cm) wide. \n\n The root system is extensive and will break containers if it needs more room. This root system must be observed on an annual basis in order to re-pot before pot breakage occurs.',
                commonProblems: const [
                  'Bugs will occasionally eat the green parts of the leaves. Be alert for insects and remove them as needed.',
                  'Blight (plant disease) can occasionally be set up in a plant. It will usually begin in one leaf, turning it brown and rotten. Remove this stem from the plant to prevent it from spreading to the entire plant.',
                  'Leaves that have been exposed to too much direct sunlight will sunburn and turn yellow. Trim sunburned leaves off of the plant and move the plant to a shadier location immediately.'
                ],
                facts: PlantFacts(
                    origin: 'North Africa.',
                    names:
                        'Aluminum PlantInfo (common). Pilea Cadierei (botanical/scientific).',
                    maxGrowth: 'Height 1 – 2 ft.',
                    poisonousForPets: 'Toxic for cats and dogs.'),
                care: PlantCare(
                    temperature:
                        'Pilea cadierei requires a year-round temperature between 60-75 ºF (15-23 ºC). It can tolerate short periods of time outside of this temperature range, but continued exposure will kill the plant.',
                    light:
                        'This plant requires at least four hours of indirect, but bright, sunlight a day. Do not permit this plant to be exposed to overly bright or direct sunlight. Too much sun will burn the leaves and cause the green parts to turn brown.',
                    watering:
                        'The Aluminum plant needs to be watered differently during different times of the year. During spring and summer, the top quarter inch of the soil should be kept moist. During fall and winter, allow the top quarter inch of soil to dry out before watering again. Do not permit water to stand in the saucer underneath the plant.',
                    soil:
                        'This plant prefers a sandy soil mixture to live in. Combine one part clean sand (or perlite) with two parts peat to provide the required soil content for continued health.',
                    rePotting:
                        'Check the root density of Pilea cadierei every spring. If the roots are becoming overly dense, transplant the plant to a size larger pot. If you do not, the root system will break the container in an attempt to find more room to grow.',
                    humidity:
                        'Normal air humidity will please this plant. If your home is overly dry during the winter, mist the leaves every other day with plain water.',
                    propagation:
                        'This plant is propagated through cuttings. Take the cuttings in the early spring. Allow them to rest in a water source for one week, or until roots form. Then transplant to their own container of sandy soil mixture.')),
          ),
          PlantCardInfo(
            title: 'Cast Iron PlantInfo – Aspidistra Elatior',
            descr:
                'Cast iron is its name for one reason only; it’s cast iron in strength when adapting to low light and neglect. The only way to really upset this plant is by over-watering or re-potting too often. There are two varieties with plain green and variegated (cream-colored stripes) linear-shaped leaves. The elatior can grow up to nearly 1 meter tall, once it’s fully matured.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/2_cast_iron_plant_aspidistra_elatior.jpg',
            plantData: PlantData(
              title: 'Cast Iron PlantInfo',
              preDescr:
                  'The Cast Iron plant (Aspidistra Elatior) receives its common name for its ability to withstand neglect. A nice and easy to care for foliage house plant.\n\nEven the worst plant neglecter can keep the A. elatior alive and well with its tolerant attitude to light, dry air and lack of watering.\n\nThere are still a few conditions a grower has to provide and take care of, which are very easy. Over-watering and re-potting too often seem to be the main problems that can cause this species problems.',
              descr:
                  'A native to Japan and Taiwan the Aspidistra elatior is a hardy garden plant and very easy to grow ornamental plant for indoors. It has been known to bloom tiny flowers near the base of the foliage, although this seems to be a very rare occasion for growers – so its mainly grown for its foliage.\n\nHow it looks and leaves: There is a green foliage type and a variegated that displays cream colored stripes along the outer edge and/or the center of each leaf. The base of the leaves are rolled, then opens out and narrows in to a point. The linear leaves grow over a foot long and 5 inches wide – which display prominent ribs.\n\nEach of the leaves are grown from a stem that can be separated with its roots for growing new plants. You may also want to clean the leaves with a soft sponge (and water) to keep them looking attractive and dust free.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Japan & Taiwan.',
                  names:
                      'Cast iron plant (common). Aspidistra elatior (botanical/scientific).',
                  maxGrowth: 'Height 36 in (91 cm).',
                  poisonousForPets: 'Non-toxic to cats and dogs.'),
              care: PlantCare(
                  temperature:
                      'Average warmth is fine of approximately 60-75°F (15-24°C) and no lower than 45°F (8°C). They will tolerate much lower temperatures.',
                  light:
                      'A brightly lit room is good but not essential. They’re fine with low levels of light, but it’s best to keep them out of direct sunlight which will damage the leaves.',
                  watering:
                      'Water once the top one inch of soil becomes dry. Water less during winter and do not over water. Over watering to the degree that the soil becomes soggy for a period of time – can cause the plant to die. The Aspidistra elatior is drought tolerant so if it misses being watered even for a month it will look unwell but be right back with you once care and water is given again.',
                  soil:
                      'Most well draining potting mixes will suffice and good drainage holes at the bottom of the pot is advised.',
                  rePotting:
                      'Re-pot only when needed (spring) or once every 2 – 3 years.',
                  humidity:
                      'Average house humidity is advised but the cast iron plant can tolerate dry air conditions.',
                  propagation:
                      'Propagate by division when its re-potting time.'),
            ),
          ),
          PlantCardInfo(
            title: 'Chinese Evergreen – Aglaonema',
            descr:
                'An array of hybrid plants from the aglaonema genus cultivated over the years because of their increase in popularity. The Chinese evergreen grows up to 3ft tall and displays oval-shaped leaves that grow 30cm in length. A good supply of varieties offers growers different leaf color variations. Variegated does not tolerate low light as well as the plain green variety.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/3_chinese_evergreen_aglaonema.png',
            plantData: PlantData(
              title: 'Chinese Evergreen PlantInfo - Aglaonema',
              preDescr:
                  'The Chinese evergreen is the common name used for a collection of houseplants from the Aglaonema genus – which tend to tolerate low light conditions very well.\n\nBotanical name Aglaonemas will produce flowers (these are not very showy), but they’re grown primarily for attractive leathery leaves.',
              descr:
                  'There are many hybrid and cultivars of the Chinese evergreen plants available that have been cultivated over the last century. This is because of their increasing popularity for indoor growers to use them as ornamental houseplants for room decoration.\n\nThese slow-growing plant varieties include plain green, speckled, blotched, and variegated types. One of the most popular and sought-after is the Silver Queen. It has leaves covered in silver mainly with some small green patches.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Tropics and subtropics of Asia.',
                  names:
                      'Chinese evergreen (common). — A. commutatum, A modestum, A pictum, and others. (botanical/scientific).',
                  maxGrowth: 'Height 3ft.',
                  poisonousForPets: 'Toxic to cats, dogs, and horses.'),
              care: PlantCare(
                  temperature:
                      'Temperatures between 65-80 ºF (18-27 ºC) are ideal. Lower than 60ºF (15ºC) is not healthy for this plant and can cause dark patches on the leaves. If the leaves begin to curl and the edges turn brown the temperature is probably too low or cold drafts may be affecting the plant.',
                  light:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  watering:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  soil:
                      'A peat-based potting soil mixed with part perlite or sand to improve drainage is ideal medium or any other well-draining potting mix.',
                  rePotting:
                      'Re-pot once every two or three years during spring. They like to become slightly root bound – so don’t worry if they seem to show some roots through the bottom of the pot.',
                  humidity:
                      'Average to high humidity is needed. Increasing the humidity levels of a room “especially if the room has artificial heating” will improve the plant’s growth and prevent leaves from becoming dry and shrilling up. This can be done by spraying or a pebble tray.',
                  propagation:
                      'These are best propagated by dividing the root with a few stalks and leaves attached during the growing season of spring and summer. Stem cuttings may also be used.'),
            ),
          ),
        ]),
    PlantTypeCardInfo(
        name: 'Common House Plants Types',
        imagePath: 'assets/images/plants/types/7_common_house_plants_types.jpg',
        plants: [
          PlantCardInfo(
            title: 'Aluminum PlantInfo – Pilea Cadierei',
            descr:
                'The aluminum plant is an easy-to-grow species native to China and Vietnam. It’s a bush-type plant that grows up to 12 inches tall and displays glossy green and silver oval-shaped leaves. Place the pilea cadierei in a brightly lit spot with sunlight to encourage it to thrive and grow well. Pruning each spring promotes new growth and spread.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/1_aluminum_plant_pilea_cadierei.jpg',
            plantData: PlantData(
                title: 'Aluminum PlantInfo',
                preDescr:
                    'The Aluminum plant is an easy-going house plant that is generally simple to please. So long as the Pilea cadierei plant gets the water and sunlight it wants it will continue to be a stunning addition to your indoor garden for years to come. \n\n Featuring wide bushy leaves that are marked with silver patterns, this native to China and Vietnam does wonderfully as a house plant.',
                descr:
                    'With the aluminum plants easy going nature and lack of any real growing problems, it will bush out from its container in leaves of silver and green for up to four years before it dies. The leaves are normally squat and broad, the body measuring up to 12 inches (30 cm) long and up to 8 inches (21 cm) wide. \n\n The root system is extensive and will break containers if it needs more room. This root system must be observed on an annual basis in order to re-pot before pot breakage occurs.',
                commonProblems: const [
                  'Bugs will occasionally eat the green parts of the leaves. Be alert for insects and remove them as needed.',
                  'Blight (plant disease) can occasionally be set up in a plant. It will usually begin in one leaf, turning it brown and rotten. Remove this stem from the plant to prevent it from spreading to the entire plant.',
                  'Leaves that have been exposed to too much direct sunlight will sunburn and turn yellow. Trim sunburned leaves off of the plant and move the plant to a shadier location immediately.'
                ],
                facts: PlantFacts(
                    origin: 'North Africa.',
                    names:
                        'Aluminum PlantInfo (common). Pilea Cadierei (botanical/scientific).',
                    maxGrowth: 'Height 1 – 2 ft.',
                    poisonousForPets: 'Toxic for cats and dogs.'),
                care: PlantCare(
                    temperature:
                        'Pilea cadierei requires a year-round temperature between 60-75 ºF (15-23 ºC). It can tolerate short periods of time outside of this temperature range, but continued exposure will kill the plant.',
                    light:
                        'This plant requires at least four hours of indirect, but bright, sunlight a day. Do not permit this plant to be exposed to overly bright or direct sunlight. Too much sun will burn the leaves and cause the green parts to turn brown.',
                    watering:
                        'The Aluminum plant needs to be watered differently during different times of the year. During spring and summer, the top quarter inch of the soil should be kept moist. During fall and winter, allow the top quarter inch of soil to dry out before watering again. Do not permit water to stand in the saucer underneath the plant.',
                    soil:
                        'This plant prefers a sandy soil mixture to live in. Combine one part clean sand (or perlite) with two parts peat to provide the required soil content for continued health.',
                    rePotting:
                        'Check the root density of Pilea cadierei every spring. If the roots are becoming overly dense, transplant the plant to a size larger pot. If you do not, the root system will break the container in an attempt to find more room to grow.',
                    humidity:
                        'Normal air humidity will please this plant. If your home is overly dry during the winter, mist the leaves every other day with plain water.',
                    propagation:
                        'This plant is propagated through cuttings. Take the cuttings in the early spring. Allow them to rest in a water source for one week, or until roots form. Then transplant to their own container of sandy soil mixture.')),
          ),
          PlantCardInfo(
            title: 'Cast Iron PlantInfo – Aspidistra Elatior',
            descr:
                'Cast iron is its name for one reason only; it’s cast iron in strength when adapting to low light and neglect. The only way to really upset this plant is by over-watering or re-potting too often. There are two varieties with plain green and variegated (cream-colored stripes) linear-shaped leaves. The elatior can grow up to nearly 1 meter tall, once it’s fully matured.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/2_cast_iron_plant_aspidistra_elatior.jpg',
            plantData: PlantData(
              title: 'Cast Iron PlantInfo',
              preDescr:
                  'The Cast Iron plant (Aspidistra Elatior) receives its common name for its ability to withstand neglect. A nice and easy to care for foliage house plant.\n\nEven the worst plant neglecter can keep the A. elatior alive and well with its tolerant attitude to light, dry air and lack of watering.\n\nThere are still a few conditions a grower has to provide and take care of, which are very easy. Over-watering and re-potting too often seem to be the main problems that can cause this species problems.',
              descr:
                  'A native to Japan and Taiwan the Aspidistra elatior is a hardy garden plant and very easy to grow ornamental plant for indoors. It has been known to bloom tiny flowers near the base of the foliage, although this seems to be a very rare occasion for growers – so its mainly grown for its foliage.\n\nHow it looks and leaves: There is a green foliage type and a variegated that displays cream colored stripes along the outer edge and/or the center of each leaf. The base of the leaves are rolled, then opens out and narrows in to a point. The linear leaves grow over a foot long and 5 inches wide – which display prominent ribs.\n\nEach of the leaves are grown from a stem that can be separated with its roots for growing new plants. You may also want to clean the leaves with a soft sponge (and water) to keep them looking attractive and dust free.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Japan & Taiwan.',
                  names:
                      'Cast iron plant (common). Aspidistra elatior (botanical/scientific).',
                  maxGrowth: 'Height 36 in (91 cm).',
                  poisonousForPets: 'Non-toxic to cats and dogs.'),
              care: PlantCare(
                  temperature:
                      'Average warmth is fine of approximately 60-75°F (15-24°C) and no lower than 45°F (8°C). They will tolerate much lower temperatures.',
                  light:
                      'A brightly lit room is good but not essential. They’re fine with low levels of light, but it’s best to keep them out of direct sunlight which will damage the leaves.',
                  watering:
                      'Water once the top one inch of soil becomes dry. Water less during winter and do not over water. Over watering to the degree that the soil becomes soggy for a period of time – can cause the plant to die. The Aspidistra elatior is drought tolerant so if it misses being watered even for a month it will look unwell but be right back with you once care and water is given again.',
                  soil:
                      'Most well draining potting mixes will suffice and good drainage holes at the bottom of the pot is advised.',
                  rePotting:
                      'Re-pot only when needed (spring) or once every 2 – 3 years.',
                  humidity:
                      'Average house humidity is advised but the cast iron plant can tolerate dry air conditions.',
                  propagation:
                      'Propagate by division when its re-potting time.'),
            ),
          ),
          PlantCardInfo(
            title: 'Chinese Evergreen – Aglaonema',
            descr:
                'An array of hybrid plants from the aglaonema genus cultivated over the years because of their increase in popularity. The Chinese evergreen grows up to 3ft tall and displays oval-shaped leaves that grow 30cm in length. A good supply of varieties offers growers different leaf color variations. Variegated does not tolerate low light as well as the plain green variety.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/3_chinese_evergreen_aglaonema.png',
            plantData: PlantData(
              title: 'Chinese Evergreen PlantInfo - Aglaonema',
              preDescr:
                  'The Chinese evergreen is the common name used for a collection of houseplants from the Aglaonema genus – which tend to tolerate low light conditions very well.\n\nBotanical name Aglaonemas will produce flowers (these are not very showy), but they’re grown primarily for attractive leathery leaves.',
              descr:
                  'There are many hybrid and cultivars of the Chinese evergreen plants available that have been cultivated over the last century. This is because of their increasing popularity for indoor growers to use them as ornamental houseplants for room decoration.\n\nThese slow-growing plant varieties include plain green, speckled, blotched, and variegated types. One of the most popular and sought-after is the Silver Queen. It has leaves covered in silver mainly with some small green patches.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Tropics and subtropics of Asia.',
                  names:
                      'Chinese evergreen (common). — A. commutatum, A modestum, A pictum, and others. (botanical/scientific).',
                  maxGrowth: 'Height 3ft.',
                  poisonousForPets: 'Toxic to cats, dogs, and horses.'),
              care: PlantCare(
                  temperature:
                      'Temperatures between 65-80 ºF (18-27 ºC) are ideal. Lower than 60ºF (15ºC) is not healthy for this plant and can cause dark patches on the leaves. If the leaves begin to curl and the edges turn brown the temperature is probably too low or cold drafts may be affecting the plant.',
                  light:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  watering:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  soil:
                      'A peat-based potting soil mixed with part perlite or sand to improve drainage is ideal medium or any other well-draining potting mix.',
                  rePotting:
                      'Re-pot once every two or three years during spring. They like to become slightly root bound – so don’t worry if they seem to show some roots through the bottom of the pot.',
                  humidity:
                      'Average to high humidity is needed. Increasing the humidity levels of a room “especially if the room has artificial heating” will improve the plant’s growth and prevent leaves from becoming dry and shrilling up. This can be done by spraying or a pebble tray.',
                  propagation:
                      'These are best propagated by dividing the root with a few stalks and leaves attached during the growing season of spring and summer. Stem cuttings may also be used.'),
            ),
          ),
        ]),
    PlantTypeCardInfo(
        name: 'Flowering House Plants',
        imagePath: 'assets/images/plants/types/8_flowering_house_plants.jpg',
        plants: [
          PlantCardInfo(
            title: 'Aluminum PlantInfo – Pilea Cadierei',
            descr:
                'The aluminum plant is an easy-to-grow species native to China and Vietnam. It’s a bush-type plant that grows up to 12 inches tall and displays glossy green and silver oval-shaped leaves. Place the pilea cadierei in a brightly lit spot with sunlight to encourage it to thrive and grow well. Pruning each spring promotes new growth and spread.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/1_aluminum_plant_pilea_cadierei.jpg',
            plantData: PlantData(
                title: 'Aluminum PlantInfo',
                preDescr:
                    'The Aluminum plant is an easy-going house plant that is generally simple to please. So long as the Pilea cadierei plant gets the water and sunlight it wants it will continue to be a stunning addition to your indoor garden for years to come. \n\n Featuring wide bushy leaves that are marked with silver patterns, this native to China and Vietnam does wonderfully as a house plant.',
                descr:
                    'With the aluminum plants easy going nature and lack of any real growing problems, it will bush out from its container in leaves of silver and green for up to four years before it dies. The leaves are normally squat and broad, the body measuring up to 12 inches (30 cm) long and up to 8 inches (21 cm) wide. \n\n The root system is extensive and will break containers if it needs more room. This root system must be observed on an annual basis in order to re-pot before pot breakage occurs.',
                commonProblems: const [
                  'Bugs will occasionally eat the green parts of the leaves. Be alert for insects and remove them as needed.',
                  'Blight (plant disease) can occasionally be set up in a plant. It will usually begin in one leaf, turning it brown and rotten. Remove this stem from the plant to prevent it from spreading to the entire plant.',
                  'Leaves that have been exposed to too much direct sunlight will sunburn and turn yellow. Trim sunburned leaves off of the plant and move the plant to a shadier location immediately.'
                ],
                facts: PlantFacts(
                    origin: 'North Africa.',
                    names:
                        'Aluminum PlantInfo (common). Pilea Cadierei (botanical/scientific).',
                    maxGrowth: 'Height 1 – 2 ft.',
                    poisonousForPets: 'Toxic for cats and dogs.'),
                care: PlantCare(
                    temperature:
                        'Pilea cadierei requires a year-round temperature between 60-75 ºF (15-23 ºC). It can tolerate short periods of time outside of this temperature range, but continued exposure will kill the plant.',
                    light:
                        'This plant requires at least four hours of indirect, but bright, sunlight a day. Do not permit this plant to be exposed to overly bright or direct sunlight. Too much sun will burn the leaves and cause the green parts to turn brown.',
                    watering:
                        'The Aluminum plant needs to be watered differently during different times of the year. During spring and summer, the top quarter inch of the soil should be kept moist. During fall and winter, allow the top quarter inch of soil to dry out before watering again. Do not permit water to stand in the saucer underneath the plant.',
                    soil:
                        'This plant prefers a sandy soil mixture to live in. Combine one part clean sand (or perlite) with two parts peat to provide the required soil content for continued health.',
                    rePotting:
                        'Check the root density of Pilea cadierei every spring. If the roots are becoming overly dense, transplant the plant to a size larger pot. If you do not, the root system will break the container in an attempt to find more room to grow.',
                    humidity:
                        'Normal air humidity will please this plant. If your home is overly dry during the winter, mist the leaves every other day with plain water.',
                    propagation:
                        'This plant is propagated through cuttings. Take the cuttings in the early spring. Allow them to rest in a water source for one week, or until roots form. Then transplant to their own container of sandy soil mixture.')),
          ),
          PlantCardInfo(
            title: 'Cast Iron PlantInfo – Aspidistra Elatior',
            descr:
                'Cast iron is its name for one reason only; it’s cast iron in strength when adapting to low light and neglect. The only way to really upset this plant is by over-watering or re-potting too often. There are two varieties with plain green and variegated (cream-colored stripes) linear-shaped leaves. The elatior can grow up to nearly 1 meter tall, once it’s fully matured.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/2_cast_iron_plant_aspidistra_elatior.jpg',
            plantData: PlantData(
              title: 'Cast Iron PlantInfo',
              preDescr:
                  'The Cast Iron plant (Aspidistra Elatior) receives its common name for its ability to withstand neglect. A nice and easy to care for foliage house plant.\n\nEven the worst plant neglecter can keep the A. elatior alive and well with its tolerant attitude to light, dry air and lack of watering.\n\nThere are still a few conditions a grower has to provide and take care of, which are very easy. Over-watering and re-potting too often seem to be the main problems that can cause this species problems.',
              descr:
                  'A native to Japan and Taiwan the Aspidistra elatior is a hardy garden plant and very easy to grow ornamental plant for indoors. It has been known to bloom tiny flowers near the base of the foliage, although this seems to be a very rare occasion for growers – so its mainly grown for its foliage.\n\nHow it looks and leaves: There is a green foliage type and a variegated that displays cream colored stripes along the outer edge and/or the center of each leaf. The base of the leaves are rolled, then opens out and narrows in to a point. The linear leaves grow over a foot long and 5 inches wide – which display prominent ribs.\n\nEach of the leaves are grown from a stem that can be separated with its roots for growing new plants. You may also want to clean the leaves with a soft sponge (and water) to keep them looking attractive and dust free.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Japan & Taiwan.',
                  names:
                      'Cast iron plant (common). Aspidistra elatior (botanical/scientific).',
                  maxGrowth: 'Height 36 in (91 cm).',
                  poisonousForPets: 'Non-toxic to cats and dogs.'),
              care: PlantCare(
                  temperature:
                      'Average warmth is fine of approximately 60-75°F (15-24°C) and no lower than 45°F (8°C). They will tolerate much lower temperatures.',
                  light:
                      'A brightly lit room is good but not essential. They’re fine with low levels of light, but it’s best to keep them out of direct sunlight which will damage the leaves.',
                  watering:
                      'Water once the top one inch of soil becomes dry. Water less during winter and do not over water. Over watering to the degree that the soil becomes soggy for a period of time – can cause the plant to die. The Aspidistra elatior is drought tolerant so if it misses being watered even for a month it will look unwell but be right back with you once care and water is given again.',
                  soil:
                      'Most well draining potting mixes will suffice and good drainage holes at the bottom of the pot is advised.',
                  rePotting:
                      'Re-pot only when needed (spring) or once every 2 – 3 years.',
                  humidity:
                      'Average house humidity is advised but the cast iron plant can tolerate dry air conditions.',
                  propagation:
                      'Propagate by division when its re-potting time.'),
            ),
          ),
          PlantCardInfo(
            title: 'Chinese Evergreen – Aglaonema',
            descr:
                'An array of hybrid plants from the aglaonema genus cultivated over the years because of their increase in popularity. The Chinese evergreen grows up to 3ft tall and displays oval-shaped leaves that grow 30cm in length. A good supply of varieties offers growers different leaf color variations. Variegated does not tolerate low light as well as the plain green variety.',
            imagePath:
                'assets/images/plants/types/1_foliage_house_plants/3_chinese_evergreen_aglaonema.png',
            plantData: PlantData(
              title: 'Chinese Evergreen PlantInfo - Aglaonema',
              preDescr:
                  'The Chinese evergreen is the common name used for a collection of houseplants from the Aglaonema genus – which tend to tolerate low light conditions very well.\n\nBotanical name Aglaonemas will produce flowers (these are not very showy), but they’re grown primarily for attractive leathery leaves.',
              descr:
                  'There are many hybrid and cultivars of the Chinese evergreen plants available that have been cultivated over the last century. This is because of their increasing popularity for indoor growers to use them as ornamental houseplants for room decoration.\n\nThese slow-growing plant varieties include plain green, speckled, blotched, and variegated types. One of the most popular and sought-after is the Silver Queen. It has leaves covered in silver mainly with some small green patches.',
              commonProblems: [],
              facts: PlantFacts(
                  origin: 'Tropics and subtropics of Asia.',
                  names:
                      'Chinese evergreen (common). — A. commutatum, A modestum, A pictum, and others. (botanical/scientific).',
                  maxGrowth: 'Height 3ft.',
                  poisonousForPets: 'Toxic to cats, dogs, and horses.'),
              care: PlantCare(
                  temperature:
                      'Temperatures between 65-80 ºF (18-27 ºC) are ideal. Lower than 60ºF (15ºC) is not healthy for this plant and can cause dark patches on the leaves. If the leaves begin to curl and the edges turn brown the temperature is probably too low or cold drafts may be affecting the plant.',
                  light:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  watering:
                      'The most common advice I have been given for the Aglaonema is the darker the leaves and stalks, the less light is needed. I would say to be safe a bright room with the plant sitting in a shaded spot is best. Avoid direct sunlight.',
                  soil:
                      'A peat-based potting soil mixed with part perlite or sand to improve drainage is ideal medium or any other well-draining potting mix.',
                  rePotting:
                      'Re-pot once every two or three years during spring. They like to become slightly root bound – so don’t worry if they seem to show some roots through the bottom of the pot.',
                  humidity:
                      'Average to high humidity is needed. Increasing the humidity levels of a room “especially if the room has artificial heating” will improve the plant’s growth and prevent leaves from becoming dry and shrilling up. This can be done by spraying or a pebble tray.',
                  propagation:
                      'These are best propagated by dividing the root with a few stalks and leaves attached during the growing season of spring and summer. Stem cuttings may also be used.'),
            ),
          ),
        ]),
  ];
}
