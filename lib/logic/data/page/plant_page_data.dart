class PlantPageData {
  PlantPageData(
      {required this.title,
      required this.plantTypeIndex,
      required this.plantIndex});

  String title;
  int plantTypeIndex, plantIndex;
}
