class PlantTypePageData {
  PlantTypePageData({required this.title, required this.plantTypeIndex});

  String title;
  int plantTypeIndex;
}
