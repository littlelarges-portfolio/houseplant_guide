import 'plant_data.dart';

class PlantCardInfo {
  PlantCardInfo({
    required this.title,
    required this.descr,
    required this.imagePath,
    required this.plantData,
  });

  String title, descr, imagePath;

  PlantData plantData;
}
