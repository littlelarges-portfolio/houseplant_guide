class PlantFacts {
  PlantFacts(
      {required this.origin,
      required this.names,
      required this.maxGrowth,
      required this.poisonousForPets});

  String origin, names, maxGrowth, poisonousForPets;
}
