class PlantCare {
  PlantCare(
      {required this.temperature,
      required this.light,
      required this.watering,
      required this.soil,
      required this.rePotting,
      required this.humidity,
      required this.propagation});

  String temperature, light, watering, soil, rePotting, humidity, propagation;
}
