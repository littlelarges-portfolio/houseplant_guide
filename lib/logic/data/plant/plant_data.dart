import 'package:houseplant_guide/logic/data/plant/plant_care.dart';
import 'package:houseplant_guide/logic/data/plant/plant_facts.dart';

class PlantData {
  PlantData({
    required this.title,
    required this.preDescr,
    required this.descr,
    required this.commonProblems,
    required this.facts,
    required this.care,
  });

  String title, preDescr, descr;

  List<String> commonProblems;

  PlantFacts facts;
  PlantCare care;
}
