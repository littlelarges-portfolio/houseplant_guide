import 'package:houseplant_guide/logic/data/plant/plant_card_info.dart';

class PlantTypeCardInfo {
  PlantTypeCardInfo(
      {required this.name, required this.imagePath, required this.plants});

  String name, imagePath;
  List<PlantCardInfo> plants;
}
