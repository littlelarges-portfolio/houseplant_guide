import 'package:flutter/material.dart';
import 'package:houseplant_guide/logic/widgets/plant_page/care_table.dart';
import 'package:houseplant_guide/logic/widgets/plant_page/common_problems.dart';
import 'package:houseplant_guide/logic/widgets/plant_page/facts_table.dart';
import 'package:marquee/marquee.dart';

import '../../main.dart';

class PlantPage extends StatelessWidget {
  const PlantPage(
      {required this.title,
      required this.plantTypeIndex,
      required this.plantIndex,
      Key? key})
      : super(key: key);

  final String title;
  final int plantTypeIndex, plantIndex;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: SizedBox(
          height: 21,
          child: Marquee(
            text: title,
            scrollAxis: Axis.horizontal,
            crossAxisAlignment: CrossAxisAlignment.start,
            blankSpace: 200.0,
            velocity: 50.0,
            pauseAfterRound: const Duration(seconds: 10),
            accelerationDuration: const Duration(seconds: 3),
            accelerationCurve: Curves.linear,
            decelerationDuration: const Duration(milliseconds: 3),
            decelerationCurve: Curves.linear,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(children: [
            Text(
                appData.plantTypes[plantTypeIndex].plants[plantIndex].plantData
                    .title,
                style: Theme.of(context).textTheme.displayLarge),
            const SizedBox(height: 20),
            Text(appData.plantTypes[plantTypeIndex].plants[plantIndex].plantData
                .preDescr),
            const SizedBox(height: 20),
            Text('Description', style: Theme.of(context).textTheme.titleLarge),
            const SizedBox(height: 20),
            Text(appData
                .plantTypes[plantTypeIndex].plants[plantIndex].plantData.descr),
            const SizedBox(height: 20),
            Text('Facts', style: Theme.of(context).textTheme.titleLarge),
            const SizedBox(height: 20),
            FactsTable(
              plantTypeIndex: plantTypeIndex,
              plantIndex: plantIndex,
            ),
            const SizedBox(height: 20),
            Text('Care', style: Theme.of(context).textTheme.titleLarge),
            const SizedBox(height: 20),
            CareTable(
              plantTypeIndex: plantTypeIndex,
              plantIndex: plantIndex,
            ),
            const SizedBox(height: 20),
            appData.plantTypes[plantTypeIndex].plants[plantIndex].plantData
                    .commonProblems.isNotEmpty
                ? Column(
                    children: [
                      Text('Common Problems',
                          style: Theme.of(context).textTheme.titleLarge),
                      const SizedBox(height: 20),
                      CommonProblems(
                        plantTypeIndex: plantTypeIndex,
                        plantIndex: plantIndex,
                      ),
                    ],
                  )
                : Container(),
          ]),
        ),
      ),
    );
  }
}
