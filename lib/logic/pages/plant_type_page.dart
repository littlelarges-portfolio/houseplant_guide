import 'package:flutter/material.dart';
import 'package:houseplant_guide/logic/widgets/plant_type_page/plant_card.dart';

import '../../main.dart';

class PlantTypePage extends StatelessWidget {
  const PlantTypePage({required this.title, required this.plantTypeIndex, Key? key})
      : super(key: key);

  final String title;
  final int plantTypeIndex;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: ListView(
          children: List.generate(
              appData.plantTypes[plantTypeIndex].plants.length, (index) {
            return PlantCard(plantTypeIndex: plantTypeIndex, plantIndex: index);
          }),
        ));
  }
}
