import 'package:flutter/material.dart';
import 'package:houseplant_guide/logic/widgets/home_page/plant_type_card.dart';

import '../../main.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key, required this.title});

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Center(child: Text(widget.title)),
        ),
        body: Column(
          children: [
            const SizedBox(height: 20),
            Center(
                child: Text('House plant type',
                    style: Theme.of(context)
                        .textTheme
                        .headlineLarge
                        ?.copyWith(color: Colors.black))),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: GridView.count(
                    mainAxisSpacing: 20,
                    crossAxisSpacing: 20,
                    crossAxisCount: 2,
                    children: List.generate(appData.plantTypes.length, (index) {
                      return PlantTypeCard(
                        plantTypeName: appData.plantTypes[index].name,
                        imagePath: appData.plantTypes[index].imagePath,
                        plantTypeIndex: index,
                      );
                    })),
              ),
            ),
          ],
        ));
  }
}
