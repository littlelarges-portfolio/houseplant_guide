import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:houseplant_guide/logic/data/app_data/app_data.dart';
import 'package:houseplant_guide/logic/app_router/app_router.dart';

void main() {
  runApp(const MyApp());
}

AppData appData = AppData();
AppRouter appRouter = AppRouter();

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.red),
        useMaterial3: true,
        textTheme: GoogleFonts.questrialTextTheme(),
      ),
      routerConfig: appRouter.router,
    );
  }
}
