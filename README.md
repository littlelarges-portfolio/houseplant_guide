# Houseplant Guide App

~9 hours were spent on development

A guide app containing information about different types of house plants

<img src="https://i.imgur.com/7CRG3rF.png" width="188" height="354">
<img src="https://i.imgur.com/zswnHaf.png" width="188" height="354"> 

This information includes a description of the plant, facts about the plant, plant care, and general plant problems if present.


<img src="https://i.imgur.com/WnFX8WQ.png" width="188" height="354"> <img src="https://i.imgur.com/MrufPle.png" width="188" height="354"> <img src="https://i.imgur.com/cpnaxUf.png" width="188" height="354"><img src="https://i.imgur.com/b82V2r9.png" width="188" height="354"><img src="https://i.imgur.com/lGUFMO6.png" width="188" height="354">

Demo:

<img src="demo/houseplant_guide_demo.gif" width="188" height="354">